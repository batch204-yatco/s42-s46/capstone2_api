const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

// allow us to access routes
const userRoutes = require("./routes/userRoutes");
const productRoutes = require("./routes/productRoutes");
const orderRoutes = require("./routes/orderRoutes");

const port = process.env.PORT || 4000;
const app = express();

mongoose.connect("mongodb+srv://admin:admin123@batch204-yatcodaphne.rlp0fjo.mongodb.net/s42-s46?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

let db = mongoose.connection;
db.on("error", () => console.error.bind(console, "Error"));
db.once("open", () => console.log("Now connected to MongoDB Atlas!"));

// to allow resources to access backend app
app.use(cors());
app.use(express.json());

app.use("/users", userRoutes);
app.use("/products", productRoutes);
app.use("/orders", orderRoutes);

app.listen(port, () => {
	console.log(`API is now online on port ${port}`);
})