const express = require("express");
const router = express.Router();
const orderController = require("../controllers/orderController");
const auth = require("../auth");

// Create Order (Non-Admin)
router.post("/checkout", auth.verify, (req, res) => {

	const isAdmin = auth.decode(req.headers.authorization).isAdmin;

	if(isAdmin === false){
		let data = {
			userId: auth.decode(req.headers.authorization).id
		}
	orderController.createOrder(req.body, data).then(resultFromController => res.send(resultFromController))
	} else {
		res.send(false);
	}
});

// STRETCH GOAL
// Retrieve All Orders (Admin)
router.get("/all", auth.verify, (req, res) => {

	const isAdmin = auth.decode(req.headers.authorization).isAdmin;

	if(isAdmin === true){
	orderController.getAllOrders().then(resultFromController => res.send(resultFromController));
	} else {
		res.send(false);
	}
})

// Retrieve Authenticated User's Orders (Non-Admin)
router.get("/myOrders", auth.verify, (req, res) => {

	let data = {
		userId: auth.decode(req.headers.authorization).id
	}

	const isAdmin = auth.decode(req.headers.authorization).isAdmin

	if(isAdmin === false){
	orderController.getMyOrders(data).then(resultFromController => res.send(resultFromController));
	} else {
		res.send(false);
	}
})

module.exports = router;