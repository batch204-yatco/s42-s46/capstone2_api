const express = require("express");
const router = express.Router();
const productController = require("../controllers/productController");
const auth = require("../auth");

// Route for creating product (admin)
router.post("/create", auth.verify, (req, res) => {

	const isAdmin = auth.decode(req.headers.authorization).isAdmin;

	if(isAdmin){
		productController.addProduct(req.body).then(resultFromController => res.send(resultFromController));
	} else {
		res.send(false);
	}

});

// Route for retrieving all ACTIVE products
router.get("/", (req, res) => {
	productController.getAllActive().then(resultFromController => res.send(resultFromController));
});

// Retrieving single product
router.get("/:productId", (req, res) => {
	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController));
});

// Updating Product (admin)
router.put("/:productId", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)

	productController.updateProduct(req.params, req.body, userData).then(resultFromController => res.send(resultFromController));

});

// Archiving Product (admin)
router.put("/:productId/archive", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)


	productController.archiveProduct(req.params, req.body, userData).then(resultFromController => res.send(resultFromController));
});

// Order Product
// router.post("/order", auth.verify, (req, res) => {

// 	let data = {
// 		userId: auth.decode(req.headers.authorization).id,
// 		productId: req.body.productId,
// 		isAdmin: auth.decode(req.headers.authorization).isAdmin
// 	}

// 	productController.orderProduct(data).then(resultFromController => res.send(resultFromController));
// })

module.exports = router;

