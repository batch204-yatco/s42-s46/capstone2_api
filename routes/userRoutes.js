const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController");
const auth = require("../auth");


// User Registration
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});

// User Authentication
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
})

// Retrieve User Details
router.get("/:userId/userDetails", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	userController.getProfile(req.params).then(resultFromController => res.send(resultFromController));
});

// STRETCH GOAL
// Set User as Admin Functionality
router.put("/:userId/setAsAdmin", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)

	
	userController.setAsAdmin(req.params, req.body, userData).then(resultFromController => res.send(resultFromController));
});

module.exports = router;