const Product = require("../models/Product");
const User = require("../models/User");
const Order = require("../models/Order");


// Create Order (Non-Admin)
module.exports.createOrder = (reqBody, data) => {

		// totalAmt Calculator
		return Product.findById(reqBody.productId).then(result=>{

		if (result !== null){

		let totalAmountProduct = reqBody.quantity * result.price

		let newOrder = new Order({
			userId: data.userId,
			products: {
			productId: reqBody.productId,
			quantity: reqBody.quantity},
			totalAmount: totalAmountProduct
		});

		return newOrder.save().then((order, error) => {

			if (error){
				return false
			} else {
				return true
			}
		})
			}
		})
	}

// STRETCH GOAL
// Retrieve All Orders (Admin)
module.exports.getAllOrders = () => {
	return Order.find({}).then(result => {
		return result;
	})
}

// Retrieve Authenticated User's Orders (Non-Admin)
module.exports.getMyOrders = (data) => {
	return Order.find({userId: data.userId}).then(result => {
		console.log(data.userId)
		console.log(data)//user id
		console.log(result)//null
		return result;
	})
}
