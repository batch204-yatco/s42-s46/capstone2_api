const Product = require("../models/Product");
const User = require("../models/User");

// Create New Product (admin)
module.exports.addProduct = (reqBody) => {
	let newProduct = new Product({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price

	});

	return newProduct.save().then((product, error) => {
		if(error){
			return false
		} else {
			return true
		}
	});
}

// Controller for retrieving all ACTIVE products
module.exports.getAllActive = () => {
	return Product.find({isActive: true}).then( result => {
		return result;
	})
}

// Retrieve single product
module.exports.getProduct = (reqParams) => {
	return Product.findById(reqParams.productId).then(result => {return result
	})
}

// Updating Product (admin)
module.exports.updateProduct = (reqParams, reqBody, data) => {

	// data is payload
	return User.findById(data.id).then(result => {
		console.log(result)

		if(result.isAdmin === true){

		let updatedProduct = {
			name: reqBody.name,
			description: reqBody.description,
			price: reqBody.price
		};

		return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product, error) => {
			if(error){
				return false
			} else {
				return true
			}
		})
	
	} else {

	return false
	
	}
})	
} 

// Archive Product (admin)
module.exports.archiveProduct = (reqParams, reqBody, data) => {

	return User.findById(data.id).then(result => {

	if(result.isAdmin === true){
		let archivedProduct = {
			isActive: false
		};

		return Product.findByIdAndUpdate(reqParams.productId, archivedProduct).then((product, error) => {
			if(error){
				return false
			} else {
				return true
			}
		})
	
	} else {

	return false
	
	}
})
}

// Order Product
// module.exports.orderProduct = async (data) => {
// 	// console.log (data)
// 	if(data.isAdmin === true){
// 			return false
// 		} else {

// 		let isUserUpdated = await User.findById(data.userId).then(user => {

// 		user.products.push({
// 			productId: data.productId
// 		});

// 		return user.save().then((user, error) => {

// 			if (error){
// 				return false
// 			} else {
// 				return true
// 			}
// 		})
// 	})
// 	}
// }