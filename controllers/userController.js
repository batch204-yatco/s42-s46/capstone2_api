const User = require("../models/User");
const Product = require("../models/Product");
const Order = require("../models/Order")
const bcrypt = require("bcrypt");
const auth = require("../auth");

// User Registration 
module.exports.registerUser = (reqBody) => {
	let newUser = new User ({
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10)
	})
		console.log (reqBody)

		if(reqBody.email === "admin@mail.com"){
			newUser = new User ({
			email: reqBody.email,
			password: bcrypt.hashSync(reqBody.password, 10),
			isAdmin: true
		})
			console.log (reqBody)
			return newUser.save().then((user, error) => {
				if(error) {
					return false
				} else {
					return true
				}
			})
		} else {
			return newUser.save().then((user, error) => {
				if(error) {
					return false
				} else {
					return true
				}
			})
		}
	}


// User Authentication 
module.exports.loginUser = async (reqBody, data) => {
	
	return User.findOne({email: reqBody.email}).then(result => {

	if(result == null) {
		return false

	} else {
		const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

		if(isPasswordCorrect){
			return {access: auth.createAccessToken(result)}
		} else {
			return false
		}
	}
	})

};

// Retrieve User Details
module.exports.getProfile = (reqParams) => {

	return User.findById(reqParams.userId).then(result => {

	if(result.id !== reqParams.userId) {
		return false
	} else {
		// Reassign password to string
		result.password = "";
		return result;
		console.log (result);

	}
	})
}

// STRETCH GOAL
// Set User as Admin Functionality
module.exports.setAsAdmin = (reqParams, reqBody, data) => {

	// data is payload
	return User.findById(data.id).then(result => {
		console.log(result)

		if(result.email === "admin@mail.com"){

		let updatedUser = {
			email: reqBody.email,
			password: bcrypt.hashSync(reqBody.password, 10),
			isAdmin: true
		};

		return User.findByIdAndUpdate(reqParams.userId, updatedUser).then((user, error) => {
			if(error){
				return false
			} else {
				return true
			}
		})
	
	} else {

	return false
	
	}
})	
} 